#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


class shortener (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    URLs = {}

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, cuerpo

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        metodo,recurso,cuerpo = parsedRequest

        if metodo == "POST":
            if (cuerpo.split('=')[1][0]) != "&" and cuerpo.split('=')[2] != "":
                clave = cuerpo.split("=")[2].replace("%2F", "/")
                self.URLs[clave] = cuerpo.split("=")[1].split("&")[0].replace('%3A', ':').replace("%2F", '/')

                if (self.URLs[clave][0:8] != "https://" and self.URLs[clave][0:7] != "http://"):

                    URLNueva = "https://" + self.URLs[clave]
                    self.URLs[clave] = URLNueva

                    httpCode = "200 OK"
                    htmlBody = "<html>" \
                                    "<body>" \
                                        "<p> SHORT: " + \
                                        str(self.URLs.keys()).split('[')[1].split(']')[0].replace(',',' ').replace("'","",2) + "</p>" \
                                        "<p> URLS: " + \
                                        str(self.URLs.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'","") + "</p>" \
                                        "<form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''>" \
                                            "<p>SHORT (empezando con /): " \
                                            "<input type = 'text' name = 'nombre' value = ''/></p>" \
                                            "<input type = 'submit' value = 'Enviar' size = '40'>" \
                                        "</form>" \
                                        "<br>" \
                                        "<a href = '" + \
                                        self.URLs[clave] + "'>'" + clave + "'</a>" \
                                        "<br>" \
                                        "<a href = '" + self.URLs[clave] + "'>'" + self.URLs[clave] + "'</a>" \
                                    "</body>" \
                               "</html>"
                else:
                    httpCode = "200 OK"
                    htmlBody = "<html>" \
                                    "<body>" \
                                        "<p> SHORT: " + \
                                        str(self.URLs.keys()).split('[')[1].split(']')[0].replace(',',' ').replace("'","",2) + "</p>" \
                                        "<p> URLS: " + \
                                        str(self.URLs.values()).split('[')[1].split(']')[0].replace(',', ' ').replace("'","") + "</p>" \
                                        "<form action = '' method = 'POST' ><p>URL: <input type = 'text' name = 'nombre' value = ''>" \
                                            "<p>SHORT (empezando con /): " \
                                            "<input type = 'text' name = 'nombre' value = ''/></p>" \
                                            "<input type = 'submit' value = 'Enviar' size = '40'>" \
                                        "</form>" \
                                        "<br>" \
                                        "<a href = '" + self.URLs[clave] + "'>'" + clave + "'</a>" \
                                        "<br>" \
                                        "<a href = '" + self.URLs[clave] + "'>'" + self.URLs[clave] + "'</a>" \
                                    "</body>" \
                               "</html>"
            else:
                httpCode = "404 ERROR"
                htmlBody = "<html>" \
                                "<body>" \
                                    "<p>Error 404</p>" \
                                "</body>" \
                           "</html>"

            return httpCode, htmlBody

        if recurso in self.URLs.keys() and recurso != "/":
            httpCode = "301 Redirect"
            htmlBody = "<html>" \
                           "<body>" \
                               "<p>Nueva URL: " + str(self.URLs.values()).split('[')[1].split(']')[0].replace(',',' ') + "</p>" \
                                "<p>URL acortada: " + str(self.URLs.keys()).split('[')[1].split(']')[0].replace(',',' ') + "</p>" \
                                "<p>Se le redireccionara a la pagina indicada en 3 segundos: </p>" \
                                "<html>" \
                                    "<body>" \
                                        "<form action = ' ' method = 'POST' >" \
                                            "<p>URL: <input type = 'text' name = 'nombre' value = ''/>""</p>" \
                                            "<p>SHORT (empezando con /): <input type = 'text' name = 'nombre' value = ''/>""</p>" \
                                            "<input type = 'submit' value = 'Enviar' size = '40'>" \
                                        "</form>" \
                                        "<meta http-equiv = 'Refresh' content = '3; url=" + self.URLs[recurso] + "'>" \
                                    "</body>" \
                                "</html>"
        elif recurso == "/":
            httpCode = "200 OK"
            htmlBody =  "<html>" \
                             "<body>" \
                                "<form action = ' ' method = 'POST' >" \
                                    "<p>URL: <input type = 'text' name = 'nombre' value = ''/>""</p>" \
                                    "<p>SHORT (empezando con /): <input type = 'text' name = 'nombre' value = ''/>""</p>" \
                                    "<input type = 'submit' value = 'Enviar' size = '40'>" \
                                "</form>" \
                            "</body>" \
                        "</html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "<html>" \
                           "<body>" \
                               "<p>Http ERROR 404 </p>" + \
                               "<html>" + \
                                    "<body>" + \
                                        "<form action = ' ' method = 'POST' >" \
                                            "<p>URL: <input type = 'text' name = 'nombre' value = ''/>""</p>" \
                                            "<p>SHORT (empezando con /): <input type = 'text' name = 'nombre' value = ''/>""</p>" \
                                            "<input type = 'submit' value = 'Enviar' size = '40'>" \
                                        "</form>" \
                                    "</body>" \
                                "</html>"

        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = shortener("localhost", 1234)
